﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Translate : MonoBehaviour {

    public Transform transform;

    public float speed = 5f;

	// Use this for initialization
	void Start () {

        transform = GetComponent<Transform>();
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButton("Fire1"))
        {

            transform.Translate(Vector3.forward * speed * Time.deltaTime);

        }
		
	}
}
